module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// object to store loaded chunks
/******/ 	// "0" means "already loaded"
/******/ 	var installedChunks = {
/******/ 		20: 0
/******/ 	};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}

/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		// "0" is the signal for "already loaded"
/******/ 		if(installedChunks[chunkId] !== 0) {
/******/ 			var chunk = require("./" + chunkId + ".server-bundle.js");
/******/ 			var moreModules = chunk.modules, chunkIds = chunk.ids;
/******/ 			for(var moduleId in moreModules) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 			for(var i = 0; i < chunkIds.length; i++)
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 		}
/******/ 		return Promise.resolve();
/******/ 	};

/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/static/";

/******/ 	// uncatched error handler for webpack runtime
/******/ 	__webpack_require__.oe = function(err) {
/******/ 		process.nextTick(function() {
/******/ 			throw err; // catch this error by using System.import().catch()
/******/ 		});
/******/ 	};

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 93);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "5ea1a87f12eaffc3262e.png";

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "b181a550a2541061103d.png";

/***/ },
/* 3 */
/***/ function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPBAMAAADJ+Ih5AAAAHlBMVEVHcEzx8fHv7+/w8PDw8PDx8fHw8PDv7+/w8PDx8fGjHRi8AAAACXRSTlMAzBD9EWKdUqD3gGn3AAAANklEQVQI12NgQAA1BQjNJCkEYbBbToQKBc+ECrFiCnHMnAYTEYGpcUARYIcJMEmKwOxyYEAGAKZICjy7avqcAAAAAElFTkSuQmCC"

/***/ },
/* 4 */
/***/ function(module, exports) {

module.exports = require("vue");

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_store_store__ = __webpack_require__(37);
/* harmony export (binding) */ __webpack_require__.d(exports, "b", function() { return app; });
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };





var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(_extends({
  store: __WEBPACK_IMPORTED_MODULE_2_store_store__["a" /* default */]
}, __WEBPACK_IMPORTED_MODULE_1_app___default.a));

/* harmony reexport (binding) */ __webpack_require__.d(exports, "a", function() { return __WEBPACK_IMPORTED_MODULE_2_store_store__["a"]; });


/***/ },
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ exports["default"] = {
	name: "about-item",
	data: function data() {
		return {
			imageSrc: ""
		};
	},

	props: {
		index: {
			type: Number,
			required: true
		},
		title: {
			type: String,
			required: true
		},
		description: {
			type: String,
			required: true
		}
	},
	mounted: function mounted() {
		var _this = this;

		return _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
			return regeneratorRuntime.wrap(function _callee$(_context) {
				while (1) {
					switch (_context.prev = _context.next) {
						case 0:
							_context.next = 2;
							return __webpack_require__(90)("./number-" + (_this.index + 1) + ".png");

						case 2:
							_this.imageSrc = _context.sent;

						case 3:
						case "end":
							return _context.stop();
					}
				}
			}, _callee, _this);
		}))();
	}
};

/***/ },
/* 25 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__about_json__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__about_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__about_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_item_vue__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_item_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__about_item_vue__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ exports["default"] = {
	name: "about",
	data: function data() {
		return {
			aboutList: __WEBPACK_IMPORTED_MODULE_0__about_json___default.a
		};
	},

	methods: {
		canvas: function canvas() {
			var _marked = [app].map(regeneratorRuntime.mark);

			function app() {
				var canvasObj, context, startHeight, offsetHeight;
				return regeneratorRuntime.wrap(function app$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								canvasObj = document.getElementById("mline");
								context = canvasObj.getContext("2d");

								canvasObj.height = canvasObj.offsetHeight;
								canvasObj.width = canvasObj.offsetWidth;
								//let offset = canvasObj.height/canvasObj.offsetHeight;
								startHeight = 30;

								context.beginPath();
								context.moveTo(canvasObj.width / 2, canvasObj.height / 2);
								context.lineTo(canvasObj.width / 2 - 160, startHeight + 48);
								context.lineTo(canvasObj.width, startHeight);
								context.lineWidth = 1;
								context.strokeStyle = '#3e4756';
								context.stroke();
								_context.next = 14;
								return;

							case 14:
								//32为点到索引图片的距离
								offsetHeight = document.querySelector(".xbox").offsetHeight + 32;

								context.moveTo(canvasObj.width / 2, canvasObj.height / 2);
								context.lineTo(canvasObj.width, offsetHeight + startHeight);
								context.stroke();
								_context.next = 20;
								return;

							case 20:
								context.moveTo(canvasObj.width / 2, canvasObj.height / 2);
								context.lineTo(canvasObj.width, offsetHeight * 2 + startHeight);
								context.stroke();
								_context.next = 25;
								return;

							case 25:
								context.moveTo(canvasObj.width / 2, canvasObj.height / 2);
								context.lineTo(canvasObj.width / 2 - 80, offsetHeight * 3 + startHeight - 48);
								context.lineTo(canvasObj.width, offsetHeight * 3 + startHeight);
								context.stroke();

							case 29:
							case "end":
								return _context.stop();
						}
					}
				}, _marked[0], this);
			}
			var iterator = app();
			return iterator;
		}
	},
	mounted: function mounted() {
		//			let canvas = this.canvas();
		//			canvas.next();
		//			canvas.next();
		//			canvas.next();
		//			canvas.next();					
	},

	components: {
		AboutItem: __WEBPACK_IMPORTED_MODULE_1__about_item_vue___default.a
	}
};

/***/ },
/* 26 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styles_reset_less__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styles_reset_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styles_reset_less__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styles_common_less__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styles_common_less___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styles_common_less__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_join_us__ = __webpack_require__(73);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_join_us___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_join_us__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_contact_us__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_contact_us___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_contact_us__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_departments__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_departments___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_departments__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_header__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_header___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_header__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_about_us__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_about_us___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_about_us__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_research_findings__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_research_findings___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_research_findings__);
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//












/* harmony default export */ exports["default"] = {
	data: function data() {
		return {
			menu: [1, 2, 3, 4, 5],
			active: 1,
			loading: true
		};
	},

	components: {
		AppHeader: __WEBPACK_IMPORTED_MODULE_5_header___default.a, Departments: __WEBPACK_IMPORTED_MODULE_4_departments___default.a, AboutUs: __WEBPACK_IMPORTED_MODULE_6_about_us___default.a, JoinUs: __WEBPACK_IMPORTED_MODULE_2_join_us___default.a, ContactUs: __WEBPACK_IMPORTED_MODULE_3_contact_us___default.a, Research: __WEBPACK_IMPORTED_MODULE_7_research_findings___default.a
	},
	methods: {
		move: function move(index) {
			fullpage.moveTo(index);
		}
	},
	mounted: function mounted() {
		var _this = this;

		return _asyncToGenerator(regeneratorRuntime.mark(function _callee4() {
			var self, links, scrollToTop, pages;
			return regeneratorRuntime.wrap(function _callee4$(_context4) {
				while (1) {
					switch (_context4.prev = _context4.next) {
						case 0:
							self = _this;
							links = document.querySelectorAll(".nav-link");

							scrollToTop = function () {
								var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(target) {
									var Velocity, html, hasClass;
									return regeneratorRuntime.wrap(function _callee$(_context) {
										while (1) {
											switch (_context.prev = _context.next) {
												case 0:
													hasClass = function hasClass(className, node) {
														var classNames = node.className.split(/\s+/);
														for (var i = 0; i < classNames.length; i++) {
															if (classNames[i] == className) {
																return true;
															}
														}
														return false;
													};

													_context.next = 3;
													return __webpack_require__.e/* System.import */(0/* duplicate */).then(__webpack_require__.bind(null, 0));

												case 3:
													Velocity = _context.sent;
													html = document.querySelector("html");

													if (!hasClass('velocity-animating', html)) {
														_context.next = 7;
														break;
													}

													return _context.abrupt("return", false);

												case 7:
													_context.next = 9;
													return Velocity(html, 'scroll', {
														duration: 750,
														offset: target.offsetTop
													});

												case 9:
												case "end":
													return _context.stop();
											}
										}
									}, _callee, _this);
								}));

								return function scrollToTop(_x) {
									return _ref.apply(this, arguments);
								};
							}();
							//＝>关于我们


							links[0].addEventListener("click", function () {
								var target = document.querySelector("section[data-id='2']");
								scrollToTop(target);
							});
							//=>研究成果
							links[1].addEventListener("click", function () {
								var target = document.querySelector("section[data-id='3']");
								scrollToTop(target);
							});
							//=>加入我们		
							links[2].addEventListener("click", function () {
								var target = document.querySelector("section[data-id='4']");
								scrollToTop(target);
							});

							_context4.next = 8;
							return __webpack_require__.e/* System.import */(19).then(__webpack_require__.bind(null, 94));

						case 8:
							pages = document.querySelectorAll(".page");

							pages.forEach(function (object) {
								//关于我们
								if (object.getAttribute("data-id") == 2) {
									new Waypoint({
										element: object,
										handler: function () {
											var _ref2 = _asyncToGenerator(regeneratorRuntime.mark(function _callee2(direction) {
												var Velocity, list, canvas;
												return regeneratorRuntime.wrap(function _callee2$(_context2) {
													while (1) {
														switch (_context2.prev = _context2.next) {
															case 0:
																_context2.next = 2;
																return __webpack_require__.e/* System.import */(0/* duplicate */).then(__webpack_require__.bind(null, 0));

															case 2:
																Velocity = _context2.sent;
																list = document.querySelectorAll(".list-box");

																if (!(list[0].style.lef && (list[0].style.left == 0 || list[0].style.left == '0px'))) {
																	_context2.next = 6;
																	break;
																}

																return _context2.abrupt("return", false);

															case 6:
																canvas = self.$refs.aboutus.canvas();
																_context2.next = 9;
																return Velocity(list[0], {
																	left: "0",
																	duration: 400,
																	easing: "swing"
																});

															case 9:
																_context2.next = 11;
																return canvas.next();

															case 11:
																_context2.next = 13;
																return Velocity(list[1], {
																	left: "0",
																	duration: 400,
																	easing: "swing"
																});

															case 13:
																_context2.next = 15;
																return canvas.next();

															case 15:
																_context2.next = 17;
																return Velocity(list[2], {
																	left: "0",
																	duration: 400,
																	easing: "swing"
																});

															case 17:
																_context2.next = 19;
																return canvas.next();

															case 19:
																_context2.next = 21;
																return Velocity(list[3], {
																	left: "0",
																	duration: 400,
																	easing: "swing"
																});

															case 21:
																_context2.next = 23;
																return canvas.next();

															case 23:
															case "end":
																return _context2.stop();
														}
													}
												}, _callee2, this);
											}));

											function handler(_x2) {
												return _ref2.apply(this, arguments);
											}

											return handler;
										}(),
										offset: 0
									});
									/*	
         		new Waypoint({
         			element: object,
         			handler: async function (direction) {
         				 const Velocity = await System.import("velocity-animate");								
         				 let list = document.querySelectorAll(".list-box");
         				 Velocity(list,{left:"100%"})
         			},
         			offset: function() {
         				return -this.adapter.outerHeight()
         			}
         		});
         */
								}

								new Waypoint({
									element: object,
									handler: function handler(direction) {
										if (direction == 'down') {
											scrollToTop(this.element);
										}
									},
									offset: '70%'
								});
								new Waypoint({
									element: object,
									handler: function () {
										var _ref3 = _asyncToGenerator(regeneratorRuntime.mark(function _callee3(direction) {
											var next, nextDom;
											return regeneratorRuntime.wrap(function _callee3$(_context3) {
												while (1) {
													switch (_context3.prev = _context3.next) {
														case 0:
															if (direction == 'up') {
																next = object.getAttribute("data-id") - 1;
																nextDom = document.querySelector("section[data-id='" + next + "']");

																if (nextDom) {
																	scrollToTop(nextDom);
																}
															}

														case 1:
														case "end":
															return _context3.stop();
													}
												}
											}, _callee3, this);
										}));

										function handler(_x3) {
											return _ref3.apply(this, arguments);
										}

										return handler;
									}(),
									offset: '30%'
								});
							});

							_this.loading = false;

						case 11:
						case "end":
							return _context4.stop();
					}
				}
			}, _callee4, _this);
		}))();
	}
};

/***/ },
/* 27 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ exports["default"] = {
	name: "contact-us"
};

/***/ },
/* 28 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__moon__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__moon___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__moon__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ exports["default"] = {
	name: "department",
	props: {
		name: {
			type: String,
			required: true
		},
		cnName: {
			type: String,
			required: true
		},
		responsibilities: {
			type: Array,
			required: true
		},
		expand: {
			type: Boolean,
			required: true
		}
	},
	components: {
		Moon: __WEBPACK_IMPORTED_MODULE_0__moon___default.a
	}
};

/***/ },
/* 29 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__department__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__department___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__department__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__department_json__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__department_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__department_json__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ exports["default"] = {
  name: "departments",
  data: function data() {
    return {
      departments: __WEBPACK_IMPORTED_MODULE_1__department_json___default.a,
      expand: false,
      clickBanned: false
    };
  },

  computed: {
    sorted: function sorted() {
      return this.departments.slice().sort(function (x, y) {
        return x.index - y.index;
      });
    }
  },
  methods: {
    reset: function reset() {
      var _this = this;

      var duration = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];
      return _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
        var Velocity;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return __webpack_require__.e/* System.import */(0).then(__webpack_require__.bind(null, 0));

              case 2:
                Velocity = _context.sent;
                _context.next = 5;
                return Promise.all(_this.departments.map(function (department) {
                  return Velocity(_this.$refs[department.title][0], _extends({ opacity: 1 }, department.position), { duration: duration });
                }));

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, _this);
      }))();
    },
    show: function show(department) {
      var _this2 = this;

      return _asyncToGenerator(regeneratorRuntime.mark(function _callee2() {
        var Velocity;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.clickBanned = true;
                _context2.next = 3;
                return __webpack_require__.e/* System.import */(0/* duplicate */).then(__webpack_require__.bind(null, 0));

              case 3:
                Velocity = _context2.sent;
                _context2.next = 6;
                return Promise.all(_this2.departments.map(function (d) {
                  if (d === department) {
                    return Velocity(_this2.$refs[d.title][0], { translateX: 0 }, { duration: 1000 });
                  } else {
                    return Velocity(_this2.$refs[d.title][0], { opacity: 0 }, { duration: 1000 });
                  }
                }));

              case 6:
                _this2.expand = _this2.departments.indexOf(department) + 1;

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, _this2);
      }))();
    },
    left: function left() {
      var _this3 = this;

      return _asyncToGenerator(regeneratorRuntime.mark(function _callee3() {
        var next, Velocity;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                next = (_this3.expand + 1) % 3;

                _this3.expand = false;
                _context3.next = 4;
                return __webpack_require__.e/* System.import */(0/* duplicate */).then(__webpack_require__.bind(null, 0));

              case 4:
                Velocity = _context3.sent;
                _context3.next = 7;
                return _this3.reset(1000);

              case 7:
                _context3.next = 9;
                return _this3.show(_this3.departments[next]);

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, _this3);
      }))();
    },
    right: function right() {
      var _this4 = this;

      return _asyncToGenerator(regeneratorRuntime.mark(function _callee4() {
        var next, Velocity;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                next = _this4.expand % 3;

                _this4.expand = false;
                _context4.next = 4;
                return __webpack_require__.e/* System.import */(0/* duplicate */).then(__webpack_require__.bind(null, 0));

              case 4:
                Velocity = _context4.sent;
                _context4.next = 7;
                return _this4.reset(1000);

              case 7:
                _context4.next = 9;
                return _this4.show(_this4.departments[next]);

              case 9:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, _this4);
      }))();
    }
  },
  mounted: function mounted() {
    this.reset();
  },

  components: { Department: __WEBPACK_IMPORTED_MODULE_0__department___default.a }
};

/***/ },
/* 30 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ exports["default"] = {
  name: "moon",
  props: {
    enTitle: { type: String, required: true },
    cnTitle: { type: String, required: true },
    description: { type: Array, required: true }
  }
};

/***/ },
/* 31 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__links_json__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__links_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__links_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__nav_link__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__nav_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__nav_link__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ exports["default"] = {
  name: "app-header",
  data: function data() {
    return { links: __WEBPACK_IMPORTED_MODULE_0__links_json___default.a };
  },

  components: { NavLink: __WEBPACK_IMPORTED_MODULE_1__nav_link___default.a }
};

/***/ },
/* 32 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__about_png__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__about_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__about_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__research_png__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__research_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__research_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__join_png__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__join_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__join_png__);
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ exports["default"] = {
  name: "header-nav-link",
  props: {
    name: { type: String, required: true },
    en: { type: String, required: true },
    cn: { type: String, required: true }
  },
  computed: {
    icon: function icon() {
      return { about: __WEBPACK_IMPORTED_MODULE_0__about_png___default.a, research: __WEBPACK_IMPORTED_MODULE_1__research_png___default.a, join: __WEBPACK_IMPORTED_MODULE_2__join_png___default.a }[this.name];
    }
  },
  methods: {
    rotateOuter: function rotateOuter() {
      var _this = this;

      return _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
        var random, outer, Velocity;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                random = Math.random() * 360;
                outer = _this.$refs.outer;
                _context.next = 4;
                return __webpack_require__.e/* System.import */(0/* duplicate */).then(__webpack_require__.bind(null, 0));

              case 4:
                Velocity = _context.sent;
                _context.next = 7;
                return Velocity(outer, { rotateZ: "-1440deg" }, { duration: 0 });

              case 7:
                _context.next = 9;
                return Velocity(outer, { rotateZ: random + "deg" }, { easing: "ease", duration: 5000 });

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, _this);
      }))();
    },
    rotateMiddle: function rotateMiddle() {
      var _this2 = this;

      return _asyncToGenerator(regeneratorRuntime.mark(function _callee2() {
        var random, middle, Velocity;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                random = Math.random() * 360;
                middle = _this2.$refs.middle;
                _context2.next = 4;
                return __webpack_require__.e/* System.import */(0/* duplicate */).then(__webpack_require__.bind(null, 0));

              case 4:
                Velocity = _context2.sent;
                _context2.next = 7;
                return Velocity(middle, { rotateZ: "1080deg" }, { duration: 0 });

              case 7:
                _context2.next = 9;
                return Velocity(middle, { rotateZ: -random + "deg" }, { easing: "ease", duration: 5000 });

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, _this2);
      }))();
    }
  },
  mounted: function mounted() {
    this.rotateOuter();
    this.rotateMiddle();
  }
};

/***/ },
/* 33 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__job_json__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__job_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__job_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__job__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__job___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__job__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ exports["default"] = {
  name: "join-us",
  data: function data() {
    return {
      jobList: __WEBPACK_IMPORTED_MODULE_0__job_json___default.a,
      index: -1
    };
  },
  mounted: function mounted() {
    this.index = Math.floor(Math.random() * 5);
  },

  components: { Job: __WEBPACK_IMPORTED_MODULE_1__job___default.a }
};

/***/ },
/* 34 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ exports["default"] = {
  name: "job",
  props: {
    enTitle: { type: String, required: true },
    cnTitle: { type: String, required: true },
    description: { type: String, required: true },
    index: { type: Number, required: true }
  },
  computed: {
    enSmall: function enSmall() {
      return this.enTitle.slice(0, this.enTitle.lastIndexOf(" "));
    },
    enLarge: function enLarge() {
      return this.enTitle.slice(this.enTitle.lastIndexOf(" ") + 1);
    }
  }
};

/***/ },
/* 35 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ exports["default"] = {
  name: "achievement",
  data: function data() {
    return {
      index: 0,
      iconSrc: ""
    };
  },

  props: {
    parentIndex: { type: Number, required: true },
    list: { type: Array, required: true }
  },
  methods: {
    isArray: Array.isArray,
    switchIcon: function switchIcon() {
      var _this = this;

      return _asyncToGenerator(regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return __webpack_require__(91)("./" + (_this.parentIndex + 1) + "-" + (_this.index + 1) + ".png");

              case 2:
                _this.iconSrc = _context.sent;

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, _this);
      }))();
    }
  },
  watch: {
    index: function index() {
      this.switchIcon();
    }
  },
  mounted: function mounted() {
    this.switchIcon();
  }
};

/***/ },
/* 36 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__achievement_list_json__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__achievement_list_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__achievement_list_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__achievement__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__achievement___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__achievement__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ exports["default"] = {
  name: "research-findings",
  data: function data() {
    return {
      achievementList: __WEBPACK_IMPORTED_MODULE_0__achievement_list_json___default.a,
      index: 0
    };
  },

  components: { Achievement: __WEBPACK_IMPORTED_MODULE_1__achievement___default.a }
};

/***/ },
/* 37 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuex__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vuex___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_vuex__);



__WEBPACK_IMPORTED_MODULE_0_vue___default.a.use(__WEBPACK_IMPORTED_MODULE_1_vuex___default.a);

/* harmony default export */ exports["a"] = new __WEBPACK_IMPORTED_MODULE_1_vuex___default.a.Store({});

/***/ },
/* 38 */
/***/ function(module, exports) {



/***/ },
/* 39 */
/***/ function(module, exports) {



/***/ },
/* 40 */
/***/ function(module, exports) {

module.exports = [
	{
		"title": "Company",
		"description": "上海益倍嘉信息技术有限公司成立于2012年6月，注册资金2500万元，益倍嘉是由专业的信息化研发团队、业务咨询团队、设计策划团队、互联网运营推广团队构建而成的跨界型信息技术企业。"
	},
	{
		"title": "Field",
		"description": "益倍嘉是一家专业的计算机软件开发及信息化解决方案提供商，我们围绕客户的需求持续创新，在金融、电信、交通(仓储物流)、电子商务、智慧城市、智慧健康等领域构筑了端到端的解决方案体系。"
	},
	{
		"title": "Service",
		"description": "益倍嘉同时还是一家综合的互联网业务运营服务商，我们以丰富人们的沟通和生活为愿景，运用新兴互联网行业的专业经验，消除数字鸿沟，推动传统行业在互联网领域的业务拓展及迅速发展。"
	},
	{
		"title": "Idea",
		"description": "我们以诚信、协作为精神基石，期待为您提供创新、专业的解决方案。"
	}
];

/***/ },
/* 41 */
/***/ function(module, exports) {

module.exports = [
	{
		"title": "software",
		"name": "software",
		"cnName": "开发",
		"responsibilities": [
			{
				"enTitle": "Internet Integration",
				"cnTitle": "互联网综合开发",
				"description": [
					"Web / Wechat / HTML5 / App",
					"业务自动处理(查询、限付、提交、支付)",
					"HTML5互动开发",
					"信息推送 / 图文推广"
				]
			},
			{
				"enTitle": "Customer Service Application",
				"cnTitle": "客户服务应用",
				"description": [
					"多媒体呼叫中心",
					"智能客户服务",
					"客户会员 / 储值卡 / 积分 / 营销"
				]
			},
			{
				"enTitle": "Multimedia Application",
				"cnTitle": "多媒体应用集成",
				"description": [
					"多媒体播放技术开发",
					"综合互联网实现综合流程场景(直播、活动、教育等)"
				]
			}
		],
		"position": {
			"translateX": "-100%",
			"scale": 1
		},
		"index": 1
	},
	{
		"title": "design",
		"name": "design",
		"cnName": "设计",
		"responsibilities": [
			{
				"enTitle": "User Interface & Experience",
				"cnTitle": "UI&UE设计",
				"description": [
					"Web / Wechat / App / 多媒体 / Banner设计"
				]
			},
			{
				"enTitle": "Graphic Design",
				"cnTitle": "插画&平面设计",
				"description": [
					"DM / 角色设计 / 活动配置设计"
				]
			},
			{
				"enTitle": "Visual Identity",
				"cnTitle": "VI-视觉识别设计",
				"description": [
					"Logo设计 / 企业视觉识别设计(VI设计)"
				]
			}
		],
		"position": {
			"translateX": 0,
			"scale": 1
		},
		"index": 3
	},
	{
		"title": "marketing",
		"name": "social-media-marketing",
		"cnName": "运营",
		"responsibilities": [
			{
				"enTitle": "Platform Operation",
				"cnTitle": "平台运营",
				"description": [
					"微信、微博等社交平台账户运营维护"
				]
			},
			{
				"enTitle": "Integrated Marketing",
				"cnTitle": "策略咨询",
				"description": [
					"品牌社交领域策略咨询"
				]
			},
			{
				"enTitle": "Building Wechat Platform",
				"cnTitle": "数据调研",
				"description": [
					"基于消费者数据挖掘的调研服务"
				]
			},
			{
				"enTitle": "Strategic Consulting",
				"cnTitle": "整合营销",
				"description": [
					"基于社交网站的数字整合传播"
				]
			}
		],
		"position": {
			"translateX": "100%",
			"scale": 1
		},
		"index": 2
	}
];

/***/ },
/* 42 */
/***/ function(module, exports) {

module.exports = [
	{
		"name": "about",
		"en": "About us",
		"cn": "关于我们"
	},
	{
		"name": "research",
		"en": "Research findings",
		"cn": "研究成果"
	},
	{
		"name": "join",
		"en": "Join us",
		"cn": "加入我们"
	}
];

/***/ },
/* 43 */
/***/ function(module, exports) {

module.exports = [
	{
		"enTitle": "Java Software Engineer",
		"cnTitle": "JAVA软件工程师",
		"description": "从事互联网软件开发，精通java，会使用多种开源框架，具有和妹子沟通脸不红心不跳的能力"
	},
	{
		"enTitle": "Front-end Web Developer",
		"cnTitle": "前端开发工程师",
		"description": "负责应用前端的开发，与后台工程师协作完成数据交互、动态信息的展现，能够编写封装良好的前端交互组件，热爱钻研各种技术难点，主动学习前沿技术"
	},
	{
		"enTitle": "User Interface Designer",
		"cnTitle": "UID / 用户界面 (UI) 设计师",
		"description": "具有优秀的审美与概念设计基础，可独立完成网页或手机的UI设计，有良好的沟通与视觉传达能力，熟练运用至少一款与专业有关的设计软件，思维不被框架局限，脑洞大"
	},
	{
		"enTitle": "Social Media Operation Specialist",
		"cnTitle": "新媒体运营专员",
		"description": "良好的策划及文字表达能力，良好的团队合作意识，良好的沟通及协调能力"
	},
	{
		"enTitle": "Copywriter Specialist",
		"cnTitle": "文案策划专员",
		"description": "熟练驾驭不同的文案风格，独立完成策划案，准确理解品牌或产品的要求和特性"
	}
];

/***/ },
/* 44 */
/***/ function(module, exports) {

module.exports = [
	{
		"title": "I2互联网管理平台",
		"introduction": "I2 (Internet Integration) 是用于企业互联网渠道管理的平台软件，综合管理各个互联网渠道。",
		"list": [
			{
				"title": "各种方式的互联网渠道的集中统一化管理",
				"description": [
					"在互联网时代，企业可以借助多种渠道终端、电子商务平台发布其产品，建立分销渠道。",
					"I2旨在建立终端渠道、公共电子商务平台与企业内部系统的沟通中枢、使企业可以管理和综合利用这些渠道，使产品能更广泛的推广和销售。"
				]
			},
			{
				"title": "多种互联网渠道下用户识别与服务",
				"description": [
					"在多商务平台接入的背景下，企业需要把握客户的真实信息和真实需求。",
					"客户购买企业的产品，可以从企业自身渠道(自建APP、商城)、公共商务平台(淘宝、京东、大众点评……)以及实体店铺、分销商等多种渠道。",
					"I2提供统一化的渠道数据接口，管理所有交易订单。在完成来自各地交易的同时，记录客户的行为轨迹。综合地针对交易行为信息汇总分析，可以计算出客户的价值与偏好取向、进而进行精准的积分兑换、会员、储值、优惠等一系列增值活动，更好的为客户服务。"
				]
			},
			{
				"title": "多种互联网渠道企业核心服务与延伸服务",
				"description": [
					"通过互联网，I2帮助企业拓展服务功能和渠道。",
					"拓展商品销售渠道，管理产品、订单、交易、服务。",
					"建立分销商统一入口，通过互联网、电子数据交换技术更好实现业务招商、分销、代理"
				]
			},
			{
				"title": "提供多种接口模式与流程模式，实现各种销售方式的接入",
				"description": [
					"线上电商模式，微店电商模式，平台电商模式，供应链分销模式"
				]
			}
		]
	},
	{
		"title": "金银生菜综合营销管理平台",
		"introduction": "金银生菜是餐饮企业的营销、管理平台，可以提供点餐、收银、预约、排队等功能。",
		"list": [
			{
				"title": "目标",
				"description": [
					[
						"一体销售",
						"线上线下统一销售管理"
					],
					[
						"渠道推广",
						"分析客户、综合互联网靶向营销"
					],
					[
						"便捷支付",
						"智能化支付服务"
					]
				]
			},
			{
				"title": "平台应用场景",
				"description": [
					"各类综合生活服务网络、各类开放营销平台、各类商务平台。"
				]
			},
			{
				"title": "开放的营销手段",
				"description": [
					[
						"搜索引擎合作渠道",
						"百度、360等"
					],
					[
						"社会化媒体合作渠道",
						"新浪微博、微信、飞信等"
					],
					[
						"同业合作、跨界合作",
						"服务运营平台(支付宝、银联钱包)",
						"生活服务网站(美团、大众点评等)"
					],
					[
						"网盟合作",
						"互联网广告等"
					]
				]
			},
			{
				"title": "灵活拓展————平台开放、拓展快速",
				"description": [
					"线上、线下、营销、支付；模块式部署。"
				]
			}
		]
	},
	{
		"title": "益倍嘉云呼叫中心平台软件/EASYCALL云呼叫中心平台管理软件",
		"introduction": "云呼叫平台是计算机与电话集成结合的呼叫中心基础平台，使用了语音网关、软交换、云技术，满足一般企业的呼叫信息管理需求，同时能够远程维护，易于拓展。",
		"list": [
			{
				"title": "益倍嘉呼叫中心业务",
				"description": [
					"分为自建式和托管式",
					"满足企业客服或电话营销需求，规范服务，减少客服培训压力，提升管理价值，提升客户服务质量与效率。",
					"实现无论身处办公室、居家还是在途，都可以随时随地通话。"
				]
			},
			{
				"title": "益倍嘉云呼叫合作价值",
				"description": [
					[
						"客户",
						"提高客户满意度，保持客户忠诚度，挖掘潜在客户，保持与提升现有客户价值。"
					],
					[
						"运营",
						"降低运营及互动成本，提高运营效率，提供准确的决策分析。"
					]
				]
			},
			{
				"title": "益倍嘉云呼叫中心优势",
				"description": [
					[
						"经济方便",
						"按使用付费，一次性投入为\"0\"，总拥有成本低；无需安装、维护，永远使用最新产品；随需而变，异地协同；移动座席，随时随地。"
					],
					[
						"安全稳定",
						"金融业安全标准与技术，保障系统安全；数据库定期备份，保证数据安全；采用多机负载均衡，保证系统运行稳定性。"
					],
					[
						"先进实用",
						"基于云计算、软交换技术的新一代呼叫中心，B/S架构，功能完善，使用方便。"
					],
					[
						"开放拓展",
						"开放API可实现与企业其他系统对接与数据融合，更好的满足企业需要；高度可扩展性，可实现功能叠加，满足企业个性化与发展需要。"
					]
				]
			}
		]
	},
	{
		"title": "益倍嘉综合营销平台软件",
		"introduction": "综合营销平台是基于规则的数据分析平台；通过在平台上配置定义积分、优惠、营销规则，灵活的实现客户权益分配策略，达到精准营销的目标。",
		"list": [
			{
				"title": "益倍嘉综合营销平台合作价值",
				"description": [
					"提供营销活动规则的制定",
					"提供积分、优惠券、活动奖品的管理",
					"提供移动端APP，推广营销业务",
					"数据分析与挖掘，辅助商业决策"
				]
			},
			{
				"title": "益倍嘉综合营销平台合作优势",
				"description": [
					"拓展营销渠道",
					"精准推送、智能推送",
					"客户资源共享、稳定客户群",
					"增加商户与客户之间的互动性",
					"提高客户计划外消费等。"
				]
			},
			{
				"title": "益倍嘉综合营销平台应用场景举例",
				"description": [
					"积分、优惠券、折扣活动等"
				]
			}
		]
	}
];

/***/ },
/* 45 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "a68e0b7bf369b9a8bb7c.png";

/***/ },
/* 46 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "c4486db55ce7f5e334d6.png";

/***/ },
/* 47 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "73aee217fd0bd5e88284.png";

/***/ },
/* 48 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "8a4ac15e56cd6148d561.png";

/***/ },
/* 49 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "89db4f098639109e1070.png";

/***/ },
/* 50 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "0517b46adbb774e1baab.png";

/***/ },
/* 51 */
/***/ function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADUAAAA1CAMAAADWOFNCAAABDlBMVEVHcEz////z8/Pv7+/x8fH////////w8PDy8vLz8/Py8vL////y8vLv7+/x8fH09PTw8PDw8PDw8PDx8fHx8fHw8PDw8PDz8/Pp6eny8vLy8vL09PTx8fH////x8fHy8vLy8vLx8fHx8fHy8vLx8fHx8fH////z8/Py8vLy8vLx8fHy8vLy8vLx8fHw8PDw8PDx8fHx8fHx8fHx8fHy8vLy8vLy8vLy8vLy8vLy8vLy8vLx8fHx8fHy8vLx8fHy8vLy8vLu7u7y8vLx8fHx8fHx8fH19fXy8vLx8fHx8fHx8fHy8vLy8vL29vbx8fH09PTw8PDz8/P////19fX////y8vL19fXy8vLy8vLy8vJJFd3bAAAAWXRSTlMABi4gwwIFIt0t2ge2EJoYIzURJfcSNhYM4Sov+AGw9O+b+Z1uSgRCPMpdVLiVM1l01tG/ubLH8WbytYeZyM7LtB58OifEGmX2u4Pe7B3BMUgsCRwI8xs9e95aED4AAAEOSURBVBgZ7cHVdsJAFAXQE0gCA8Ud6u7u7u5+/v9HKlDKrDWd3D70qeyNtn8sMT6wuLQx2oVf8GZ7+Wl5GmJON7/kJyDVx2/9GciMsNWcB5EOaoYg4XRSMwWJCHUdkFDUuZBQ1LmQUNS5kAhRtwWJA+p2PAhkqNvNIph3E6Nmfw/B4nevbHWayyHYfbVcYYvz8iGC+TUUn9h05mfTEFCPqEXZkIzjKgWBh2fghXVHgBeKQ8APObhlXQw4DkOkVC1cs+EkEfJhl97cjn6o5Nl0eRF9F1sY9GFWXKHF5DCM5mm1CpMw7dZhomjnwkTRzoWJop0LE0U7FyaFHlolYTSzRouxEsyccORHKbS1/Zk3wUnLM7gnG7QAAAAASUVORK5CYII="

/***/ },
/* 52 */
/***/ function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC4AAAAuCAMAAABgZ9sFAAAAw1BMVEVHcEzx8fHu7u7x8fHw8PDy8vLx8fHx8fHz8/Py8vLx8fHx8fHv7+/z8/Py8vLy8vLy8vLy8vL19fXx8fH////y8vL09PTy8vLy8vLz8/Px8fHx8fHv7+/y8vLx8fHx8fHy8vLx8fHy8vLw8PDx8fH////x8fHx8fHw8PD19fXy8vLy8vLu7u7x8fHy8vLy8vL////////x8fH////y8vLx8fHw8PDw8PDx8fH////x8fHz8/Px8fHy8vLy8vLy8vLy8vJ+ohdtAAAAQHRSTlMA9x/5SN9Ju0TMN/ggLtng3p4b4gQUMKCMVUzVIfWocVP7jhKEA3C8RxrG4R5LaO8GAvoBil42NcQFiS2C7p1UXAGDGAAAAMNJREFUGBntwcdWwlAUBdADJAHpxa6ACgoK9i7F8/9f5XK93GR4DwNm7I2dbWmUc+9wPT0y8wFfhyZ+hS+heYPge8XUJxRrBm1IupVggW343Qt+IFky+IKkw6AFSZnByzMUTaZuoegzVYVgRHMBQYPmBoJ7mmMIHmgGEIxp7iA4oulBMKG5vIavyUwXvgIzVbgOmDuB65C5IVynzF3BVZzRlCL4zlsMSgkU9Zj/agk09ZhkLYKq2GYhgm5+NsUm9rGzkT8KwkB2jFJDbwAAAABJRU5ErkJggg=="

/***/ },
/* 53 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "07938bbf98b68bc48efe.png";

/***/ },
/* 54 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "f65c33cecc740fc8f201.png";

/***/ },
/* 55 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "c5b94de6f25b72af5915.png";

/***/ },
/* 56 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "f1f43e287476580e9442.png";

/***/ },
/* 57 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "94fd83e69c1db9f2e749.png";

/***/ },
/* 58 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "5e7baa336b831c1e039c.png";

/***/ },
/* 59 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "7910ed66a8c4fbc9057c.png";

/***/ },
/* 60 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "6a3bf016912977654ccd.png";

/***/ },
/* 61 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "365e7f0860aa8242a1e9.png";

/***/ },
/* 62 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "323507a5ae2edd316785.png";

/***/ },
/* 63 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "7a6a61eb9618c2f49864.png";

/***/ },
/* 64 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(24)

/* template */
var __vue_template__ = __webpack_require__(78)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 65 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(25)

/* template */
var __vue_template__ = __webpack_require__(86)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 66 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(26)

/* template */
var __vue_template__ = __webpack_require__(77)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 67 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(27)

/* template */
var __vue_template__ = __webpack_require__(83)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 68 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(28)

/* template */
var __vue_template__ = __webpack_require__(89)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 69 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(29)

/* template */
var __vue_template__ = __webpack_require__(84)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 70 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(30)

/* template */
var __vue_template__ = __webpack_require__(81)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 71 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(31)

/* template */
var __vue_template__ = __webpack_require__(85)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 72 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(32)

/* template */
var __vue_template__ = __webpack_require__(80)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 73 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(33)

/* template */
var __vue_template__ = __webpack_require__(82)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 74 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(34)

/* template */
var __vue_template__ = __webpack_require__(88)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 75 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(35)

/* template */
var __vue_template__ = __webpack_require__(79)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 76 */
/***/ function(module, exports, __webpack_require__) {

var __vue_exports__, __vue_options__

/* script */
__vue_exports__ = __webpack_require__(36)

/* template */
var __vue_template__ = __webpack_require__(87)
__vue_options__ = __vue_exports__ = __vue_exports__ || {}
if (typeof __vue_exports__.default === "object") {
__vue_options__ = __vue_exports__ = __vue_exports__.default
}
if (typeof __vue_options__ === "function") {
  __vue_options__ = __vue_options__.options
}
__vue_options__.render = __vue_template__.render
__vue_options__.staticRenderFns = __vue_template__.staticRenderFns

module.exports = __vue_exports__


/***/ },
/* 77 */
/***/ function(module, exports, __webpack_require__) {

module.exports={render:function (){with(this) {
  return _h('div', {
    staticClass: "wrapper",
    attrs: {
      "id": "app"
    }
  }, [(loading) ? _h('div', {
    staticClass: "pages loader"
  }, [_m(0)]) : _e(), " ", _h('div', {
    staticClass: "pages",
    class: {
      'loading': !loading
    },
    attrs: {
      "id": "fullpage"
    }
  }, [_h('section', {
    staticClass: "page",
    attrs: {
      "data-id": "1"
    }
  }, [_h('AppHeader'), " ", _h('Departments')]), " ", _h('section', {
    staticClass: "page",
    attrs: {
      "data-id": "2"
    }
  }, [_m(1), " ", _h('AboutUs', {
    ref: "aboutus"
  }), " "]), " ", _h('section', {
    staticClass: "page",
    attrs: {
      "data-id": "3"
    }
  }, [_m(2), " ", _h('Research'), " "]), " ", " ", _h('section', {
    staticClass: "page",
    attrs: {
      "data-id": "4"
    }
  }, [_m(3), " ", _h('JoinUs'), " "]), " ", " ", _h('section', {
    staticClass: "page",
    attrs: {
      "data-id": "5"
    }
  }, [_m(4), " ", _h('ContactUs')])]), " "])
}},staticRenderFns: [function (){with(this) {
  return _h('div', {
    staticClass: "z-circle"
  }, [_h('div', {
    staticClass: "outer"
  }, [_h('div', {
    staticClass: "inner"
  }, [_h('img', {
    attrs: {
      "src": __webpack_require__(1)
    }
  })])])])
}},function (){with(this) {
  return _h('div', {
    staticClass: "title"
  }, [_h('img', {
    attrs: {
      "src": __webpack_require__(45),
      "alt": ""
    }
  })])
}},function (){with(this) {
  return _h('div', {
    staticClass: "title"
  }, [_h('img', {
    attrs: {
      "src": __webpack_require__(48),
      "alt": ""
    }
  })])
}},function (){with(this) {
  return _h('div', {
    staticClass: "title"
  }, [_h('img', {
    attrs: {
      "src": __webpack_require__(47),
      "alt": ""
    }
  })])
}},function (){with(this) {
  return _h('div', {
    staticClass: "title"
  }, [_h('img', {
    attrs: {
      "src": __webpack_require__(46),
      "alt": ""
    }
  })])
}}]}

/***/ },
/* 78 */
/***/ function(module, exports) {

module.exports={render:function (){with(this) {
  return _h('li', {
    staticClass: "list-box"
  }, [_h('span', {
    staticClass: "index"
  }, [_m(0), " ", (imageSrc) ? _h('img', {
    attrs: {
      "src": imageSrc
    }
  }) : _e()]), " ", _h('div', {
    staticClass: "order-title"
  }, [_s(title)]), " ", _h('div', {
    staticClass: "xbox"
  }, [_h('div', {
    staticClass: "inner"
  }, ["\n\t\t\t" + _s(description) + "\n\t\t"])])])
}},staticRenderFns: [function (){with(this) {
  return _h('span', {
    staticClass: "index-box"
  })
}}]}

/***/ },
/* 79 */
/***/ function(module, exports, __webpack_require__) {

module.exports={render:function (){with(this) {
  return _h('div', {
    staticClass: "achievement"
  }, [_h('div', {
    staticClass: "achievement-detail-pagination"
  }, [_h('div', {
    staticClass: "clearfix"
  }, ["\n\t\t\t\tPagination\n\t\t\t\t", _m(0), " ", _h('div', {
    staticClass: "pull-right"
  }, [_h('ul', [(list.length) && _l((list.length), function(n) {
    return _h('li', {
      staticClass: "achievement-detail-pagination-item",
      class: {
        current: n - 1 === index
      },
      on: {
        "mouseover": function($event) {
          index = n - 1
        }
      }
    })
  })])])])]), " ", _h('div', {
    staticClass: "achievement-icon-wrapper"
  }, [_h('div', {
    staticClass: "sample_circle"
  }, [_h('div', {
    staticClass: "outer"
  }, [_h('div', {
    staticClass: "inner"
  }, [_m(1), " ", (iconSrc) ? _h('img', {
    staticClass: "achievement-icon",
    attrs: {
      "src": iconSrc
    }
  }) : _e()])])])]), " ", _h('div', {
    staticClass: "achievement-detail-wrapper"
  }, [(list) && _l((list), function(item, i) {
    return _h('div', {
      directives: [{
        name: "show",
        value: (i === index),
        expression: "i === index"
      }],
      staticClass: "achievement-detail"
    }, [_h('div', {
      staticClass: "achievement-detail-title"
    }, [_s(item.title)]), " ", (item.description) && _l((item.description), function(paragraph) {
      return _h('div', {
        staticClass: "achievement-detail-list"
      }, [_h('p', {
        staticClass: "paragraph"
      }, [(isArray(paragraph)) ? [(paragraph) && _l((paragraph), function(p, j) {
        return [(j > 0) ? [_m(2)] : _e(), "\n                " + _s(p) + "\n              "]
      })] : [_s(paragraph)], " "])])
    })])
  })])])
}},staticRenderFns: [function (){with(this) {
  return _h('img', {
    staticClass: "arrow-right",
    attrs: {
      "src": __webpack_require__(3)
    }
  })
}},function (){with(this) {
  return _h('div', {
    staticClass: "xcirlce"
  })
}},function (){with(this) {
  return _h('br')
}}]}

/***/ },
/* 80 */
/***/ function(module, exports) {

module.exports={render:function (){with(this) {
  return _h('a', {
    staticClass: "nav-link",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
      }
    }
  }, [_h('div', {
    staticClass: "image"
  }, [_h('div', {
    ref: "outer",
    staticClass: "animation-wrapper"
  }, [_m(0)]), " ", _h('div', {
    ref: "middle",
    staticClass: "animation-wrapper"
  }, [_m(1)]), " ", _m(2), " ", _h('img', {
    attrs: {
      "src": icon,
      "alt": name
    }
  })]), " ", _h('div', {
    staticClass: "words"
  }, [_h('div', {
    staticClass: "en"
  }, [_s(en)]), " ", _h('div', {
    staticClass: "cn"
  }, [_s(cn)])])])
}},staticRenderFns: [function (){with(this) {
  return _h('div', {
    staticClass: "outer-wrapper"
  }, [_h('div', {
    staticClass: "outer"
  })])
}},function (){with(this) {
  return _h('div', {
    staticClass: "middle-wrapper"
  }, [_h('div', {
    staticClass: "middle"
  })])
}},function (){with(this) {
  return _h('div', {
    staticClass: "inner"
  })
}}]}

/***/ },
/* 81 */
/***/ function(module, exports) {

module.exports={render:function (){with(this) {
  return _h('li', {
    staticClass: "moon"
  }, [_m(0), " ", _m(1), " ", _m(2), " ", _h('div', {
    staticClass: "moon-detail"
  }, [_h('div', {
    staticClass: "detail-title-en"
  }, [_s(enTitle)]), " ", _h('div', {
    staticClass: "detail-title-cn"
  }, [_s(cnTitle)]), " ", (description) && _l((description), function(d) {
    return _h('div', {
      staticClass: "detail-description"
    }, [_s(d)])
  })])])
}},staticRenderFns: [function (){with(this) {
  return _h('div', {
    staticClass: "moon-outer"
  })
}},function (){with(this) {
  return _h('div', {
    staticClass: "moon-middle"
  })
}},function (){with(this) {
  return _h('div', {
    staticClass: "moon-inner"
  })
}}]}

/***/ },
/* 82 */
/***/ function(module, exports, __webpack_require__) {

module.exports={render:function (){with(this) {
  return _h('div', {
    attrs: {
      "id": "join-us"
    }
  }, [_h('div', {
    staticClass: "container"
  }, [_h('nav', {
    staticClass: "clearfix"
  }, [(index !== 0) ? _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(59)
    },
    on: {
      "mouseover": function($event) {
        index = 0
      }
    }
  }) : _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(58)
    }
  }), " ", " ", (index !== 1) ? _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(57)
    },
    on: {
      "mouseover": function($event) {
        index = 1
      }
    }
  }) : _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(56)
    }
  }), " ", " ", (index !== 2) ? _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(63)
    },
    on: {
      "mouseover": function($event) {
        index = 2
      }
    }
  }) : _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(62)
    }
  }), " ", " ", (index !== 3) ? _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(61)
    },
    on: {
      "mouseover": function($event) {
        index = 3
      }
    }
  }) : _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(60)
    }
  }), " ", " ", (index !== 4) ? _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(55)
    },
    on: {
      "mouseover": function($event) {
        index = 4
      }
    }
  }) : _h('img', {
    staticClass: "job-banner",
    attrs: {
      "src": __webpack_require__(54)
    }
  }), " "]), " ", (jobList) && _l((jobList), function(job, i) {
    return _h('job', {
      directives: [{
        name: "show",
        value: (i === index),
        expression: "i === index"
      }],
      attrs: {
        "en-title": job.enTitle,
        "cn-title": job.cnTitle,
        "description": job.description,
        "index": i
      }
    })
  })])])
}},staticRenderFns: []}

/***/ },
/* 83 */
/***/ function(module, exports, __webpack_require__) {

module.exports={render:function (){with(this) {
  return _m(0)
}},staticRenderFns: [function (){with(this) {
  return _h('main', [_h('div', {
    attrs: {
      "id": "contact-us"
    }
  }, [_h('ul', {
    staticClass: "information-list container clearfix"
  }, [_h('li', {
    staticClass: "information-item"
  }, [_h('img', {
    staticClass: "image",
    attrs: {
      "src": __webpack_require__(2)
    }
  }), " ", _h('div', {
    staticClass: "title"
  }, [_h('div', {
    staticClass: "cn"
  }, ["联系地址"]), " ", _h('div', {
    staticClass: "en"
  }, ["Contact address"])]), " ", _h('div', {
    staticClass: "detail"
  }, ["上海市长宁区延安西路1612号"])]), " ", _h('li', {
    staticClass: "information-item"
  }, [_h('img', {
    staticClass: "image",
    attrs: {
      "src": __webpack_require__(49)
    }
  }), " ", _h('div', {
    staticClass: "title"
  }, [_h('div', {
    staticClass: "cn"
  }, ["联系客服"]), " ", _h('div', {
    staticClass: "en"
  }, ["Service hotline"])]), " ", _h('div', {
    staticClass: "detail"
  }, ["400-118-8086"])]), " ", _h('li', {
    staticClass: "information-item"
  }, [_h('img', {
    staticClass: "image",
    attrs: {
      "src": __webpack_require__(2)
    }
  }), " ", _h('div', {
    staticClass: "title"
  }, [_h('div', {
    staticClass: "cn"
  }, ["电子邮件"]), " ", _h('div', {
    staticClass: "en"
  }, ["E-mail"])]), " ", _h('div', {
    staticClass: "detail"
  }, ["\n\t\t\t\t\t销售 sales@ebeijia.com\n\t\t\t\t\t", _h('br'), " 客服 service@ebeijia.com\n\t\t\t\t\t", _h('br'), " 伙伴 partner@ebeijia.com\n\t\t\t\t"])])]), " ", _h('img', {
    staticClass: "map",
    attrs: {
      "src": __webpack_require__(50)
    }
  })]), " ", _h('div', {
    staticClass: "text-center copyright"
  }, ["\n\t\t© Copyright Reserved 2016-2018   沪ICP备08104990号\n\t"])])
}}]}

/***/ },
/* 84 */
/***/ function(module, exports) {

module.exports={render:function (){with(this) {
  return _h('section', {
    attrs: {
      "id": "departments"
    }
  }, [(sorted) && _l((sorted), function(department) {
    return _h('div', {
      key: department.title,
      staticClass: "position",
      class: [department.title]
    }, [_h('div', {
      ref: department.title,
      refInFor: true,
      staticClass: "position-inner",
      on: {
        "click": function($event) {
          !clickBanned && show(department)
        }
      }
    }, [_h('department', {
      attrs: {
        "name": department.title,
        "cn-name": department.cnName,
        "responsibilities": department.responsibilities,
        "expand": !!expand
      }
    })])])
  }), " ", _h('span', {
    directives: [{
      name: "show",
      value: (expand),
      expression: "expand"
    }],
    staticClass: "arrow left",
    on: {
      "click": left
    }
  }), " ", _h('span', {
    directives: [{
      name: "show",
      value: (expand),
      expression: "expand"
    }],
    staticClass: "arrow right",
    on: {
      "click": right
    }
  })])
}},staticRenderFns: []}

/***/ },
/* 85 */
/***/ function(module, exports, __webpack_require__) {

module.exports={render:function (){with(this) {
  return _h('header', {
    staticClass: "clearfix",
    attrs: {
      "id": "header"
    }
  }, [_h('nav', [_h('div', {
    staticClass: "small-link"
  }, [_h('a', {
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
      }
    }
  }, ["官方微博"]), "\n      |\n      ", _h('a', {
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
      }
    }
  }, ["官方微信"])]), " ", _h('div', {
    staticClass: "link clearfix"
  }, [(links) && _l((links), function(link) {
    return _h('nav-link', {
      attrs: {
        "name": link.name,
        "en": link.en,
        "cn": link.cn
      }
    })
  })])]), " ", _m(0)])
}},staticRenderFns: [function (){with(this) {
  return _h('div', {
    staticClass: "logo"
  }, [_h('img', {
    attrs: {
      "src": __webpack_require__(1),
      "alt": "EBJ"
    }
  }), " ", _h('span', ["益倍嘉"])])
}}]}

/***/ },
/* 86 */
/***/ function(module, exports, __webpack_require__) {

module.exports={render:function (){with(this) {
  return _h('main', {
    attrs: {
      "id": "about"
    }
  }, [_m(0), " ", _h('div', {
    staticClass: "right-segement"
  }, [_h('ul', [(aboutList) && _l((aboutList), function(item, index) {
    return _h('aboutItem', {
      attrs: {
        "index": index,
        "title": item.title,
        "description": item.description
      }
    })
  })])])])
}},staticRenderFns: [function (){with(this) {
  return _h('div', {
    staticClass: "left-segement"
  }, [_h('div', {
    staticClass: "container"
  }, [_h('div', {
    staticClass: "z-circle"
  }, [_h('div', {
    staticClass: "outer"
  }, [_h('div', {
    staticClass: "inner"
  }, [_h('div', {
    staticClass: "last"
  }), " ", _h('img', {
    attrs: {
      "src": __webpack_require__(1)
    }
  })])])]), " ", _h('canvas', {
    staticClass: "top-line-box",
    attrs: {
      "id": "mline"
    }
  })])])
}}]}

/***/ },
/* 87 */
/***/ function(module, exports, __webpack_require__) {

module.exports={render:function (){with(this) {
  return _h('section', {
    attrs: {
      "id": "research-findings"
    }
  }, [_h('div', {
    staticClass: "container clearfix"
  }, [(achievementList) && _l((achievementList), function(achievement, i) {
    return _h('div', {
      directives: [{
        name: "show",
        value: (i === index),
        expression: "i === index"
      }]
    }, [_h('div', {
      staticClass: "achievement-title"
    }, [_s(achievement.title)]), " ", _h('div', {
      staticClass: "achievement-introduction"
    }, [_s(achievement.introduction)])])
  }), " ", _h('div', {
    staticClass: "achievements-list"
  }, [(achievementList) && _l((achievementList), function(achievement, i) {
    return _h('achievement', {
      directives: [{
        name: "show",
        value: (i === index),
        expression: "i === index"
      }],
      key: achievement.title,
      attrs: {
        "parent-index": i,
        "list": achievement.list
      }
    })
  })]), " ", _h('div', {
    staticClass: "achievement-paginate"
  }, [_h('div', {
    staticClass: "z-circle pull-left right-circle"
  }, [_h('div', {
    staticClass: "outer"
  }, [_h('div', {
    staticClass: "inner"
  }, [_m(0), _h('span', {
    attrs: {
      "style": "font-size:24px;margin-left:4px"
    }
  }, ["0" + _s(index + 1)])])])]), " ", _m(1), " ", _h('ul', [(achievementList.length) && _l((achievementList.length), function(n) {
    return _h('li', {
      staticClass: "achievement-paginate-item",
      class: {
        current: n - 1 === index
      },
      on: {
        "mouseover": function($event) {
          index = n - 1
        }
      }
    }, ["\n            0" + _s(n) + "\n            ", _m(2, true)])
  })])])])])
}},staticRenderFns: [function (){with(this) {
  return _h('i', ["DATA"])
}},function (){with(this) {
  return _h('header', {
    attrs: {
      "style": "\n    position: relative;\n"
    }
  }, ["\n          Achievements\n          ", _h('img', {
    staticClass: "arrow-down",
    attrs: {
      "src": __webpack_require__(3)
    }
  })])
}},function (){with(this) {
  return _h('span')
}}]}

/***/ },
/* 88 */
/***/ function(module, exports) {

module.exports={render:function (){with(this) {
  return _h('div', [_h('header', {
    staticClass: "job-header"
  }, [_h('div', [_s(enSmall)]), " ", _h('div', {
    staticClass: "large"
  }, [_s(enLarge)]), " ", _h('div', {
    staticClass: "joinus-list"
  }, [_h('ul', [_h('li', {
    class: {
      active: index === 0
    }
  }), " ", _h('li', {
    class: {
      active: index === 1
    }
  }), " ", _h('li', {
    class: {
      active: index === 2
    }
  }), " ", _h('li', {
    class: {
      active: index === 3
    }
  }), " ", _h('li', {
    class: {
      active: index === 4
    }
  })])])]), " ", _h('div', {
    staticClass: "job-description-wrapper xbox"
  }, [_h('div', {
    staticClass: "inner"
  }, [_h('div', {
    staticClass: "clearfix"
  }, [_h('div', {
    staticClass: "position"
  }, [_m(0), " ", _h('div', {
    staticClass: "item-content"
  }, [_s(cnTitle)])]), " ", _m(1), " ", _m(2)]), " ", _h('div', {
    staticClass: "description"
  }, [_m(3), " ", _h('div', {
    staticClass: "item-content"
  }, [_s(description)])])])])])
}},staticRenderFns: [function (){with(this) {
  return _h('div', {
    staticClass: "item-title"
  }, ["POSITION"])
}},function (){with(this) {
  return _h('div', {
    staticClass: "email"
  }, [_h('div', {
    staticClass: "item-title"
  }, ["EMAIL"]), " ", _h('div', {
    staticClass: "item-content"
  }, ["zhaopin@ebeijia.com"])])
}},function (){with(this) {
  return _h('div', {
    staticClass: "phone"
  }, [_h('div', {
    staticClass: "item-title"
  }, ["PHONE"]), " ", _h('div', {
    staticClass: "item-content"
  }, ["021-52833062"])])
}},function (){with(this) {
  return _h('div', {
    staticClass: "item-title"
  }, ["JOB DESCRIPTION"])
}}]}

/***/ },
/* 89 */
/***/ function(module, exports) {

module.exports={render:function (){with(this) {
  return _h('div', {
    staticClass: "department"
  }, [_h('div', {
    staticClass: "planet"
  }, [_h('div', {
    directives: [{
      name: "show",
      value: (expand),
      expression: "expand"
    }],
    staticClass: "detail"
  }, [_m(0), " ", _m(1), " ", _m(2), " ", _h('div', {
    staticClass: "satellite-orbit"
  }, [_h('div', {
    staticClass: "satellite-wrapper"
  }, [_h('div', {
    staticClass: "satellite"
  }, [_h('div', {
    staticClass: "title"
  }, [_s(name)]), " ", _h('div', {
    staticClass: "cn-title"
  }, [_s(cnName)])])])]), " ", _h('ul', {
    staticClass: "moon-list"
  }, [(responsibilities) && _l((responsibilities), function(responsibility) {
    return _h('moon', {
      attrs: {
        "en-title": responsibility.enTitle,
        "cn-title": responsibility.cnTitle,
        "description": responsibility.description
      }
    })
  })])]), " ", _h('span', {
    directives: [{
      name: "show",
      value: (!expand),
      expression: "!expand"
    }],
    staticClass: "words"
  })])])
}},staticRenderFns: [function (){with(this) {
  return _h('div', {
    staticClass: "outer"
  })
}},function (){with(this) {
  return _h('div', {
    staticClass: "middle"
  })
}},function (){with(this) {
  return _h('div', {
    staticClass: "inner"
  })
}}]}

/***/ },
/* 90 */
/***/ function(module, exports, __webpack_require__) {

var map = {
	"./number-1.png": [
		6,
		18
	],
	"./number-2.png": [
		7,
		17
	],
	"./number-3.png": [
		8,
		16
	],
	"./number-4.png": [
		9,
		15
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 90;


/***/ },
/* 91 */
/***/ function(module, exports, __webpack_require__) {

var map = {
	"./1-1.png": [
		10,
		14
	],
	"./1-2.png": [
		11,
		13
	],
	"./1-3.png": [
		12,
		12
	],
	"./1-4.png": [
		13,
		11
	],
	"./2-1.png": [
		14,
		10
	],
	"./2-2.png": [
		15,
		9
	],
	"./2-3.png": [
		16,
		8
	],
	"./2-4.png": [
		17,
		7
	],
	"./3-1.png": [
		18,
		6
	],
	"./3-2.png": [
		19,
		5
	],
	"./3-3.png": [
		20,
		4
	],
	"./4-1.png": [
		21,
		3
	],
	"./4-2.png": [
		22,
		2
	],
	"./4-3.png": [
		23,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 91;


/***/ },
/* 92 */
/***/ function(module, exports) {

module.exports = require("vuex");

/***/ },
/* 93 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_main__ = __webpack_require__(5);


/* harmony default export */ exports["default"] = function (context) {
  return new Promise(function (resolve) {
    context.initialState = __WEBPACK_IMPORTED_MODULE_0_main__["a" /* store */].state;
    resolve(__WEBPACK_IMPORTED_MODULE_0_main__["b" /* app */]);
  });
};

/***/ }
/******/ ]);