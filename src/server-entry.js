import {app, store} from "main";

export default context => new Promise(resolve => {
  context.initialState = store.state;
  resolve(app);
});
