import Vue from "vue";
import App from "app";
import store from "store/store";

const app = new Vue({
  store,
  ...App
});

export {app, store};
