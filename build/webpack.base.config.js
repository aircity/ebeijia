const path = require("path");
const autoprefixer = require("autoprefixer");
const {cssLoader, lessLoader, imageLoader} = require("./utilities");

module.exports = {
  entry: {
    app: "./src/client-entry.js",
    vendor: ["vue", "vuex", "lru-cache", "es6-promise"]
  },
  output: {
    path: path.resolve("static"),
    publicPath: "/static/",
    filename: "client-bundle.js"
  },
  resolveLoader: {
    root: path.resolve("node_modules"),
  },
  module: {
    loaders: [{
      test: /\.vue$/,
      loader: "vue-loader"
    }, {
      test: /\.js$/,
      loader: "babel-loader", exclude: /node_modules/
    }, {
      test: /\.json$/,
      loader: "json-loader"
    }, {
      test: /\.(png|jpg|gif|svg)$/,
      loader: imageLoader("[hash:20].[ext]")
    }]
  },
  postcss: () => [autoprefixer],
  babel: {
    presets: [
      ["es2015", {modules: false}],
      "stage-0"
    ],
    sourceMap: true
  },
  vue: {
    loaders: {
      js: "babel-loader",
      css: cssLoader,
      less: lessLoader
    }
  },
  resolve: {
    modules: ["node_modules", path.resolve("src")],
    extensions: ["", ".vue", ".js"]
  }
};
