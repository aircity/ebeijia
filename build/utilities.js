const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const develop = (process.env.NODE_ENV || "develop") === "develop";

const styleLoader = function(loaders, extract = false) {
  if (extract) {
    return ExtractTextPlugin.extract({
      fallbackLoader: loaders[0],
      loader: loaders.slice(1)
    });
  } else {
    return loaders.join("!");
  }
};

exports.styleLoader = styleLoader;

exports.cssLoader = styleLoader([
  "vue-style-loader",
  develop ? "css-loader?sourceMap" : "css-loader?-autoprefixer",
  develop ? "postcss-loader?sourceMap" : "postcss-loader"
], !develop);

exports.lessLoader = styleLoader([
  "vue-style-loader",
  develop ? "css-loader?sourceMap" : "css-loader?-autoprefixer",
  develop ? "postcss-loader?sourceMap" : "postcss-loader",
  develop ? "less-loader?sourceMap" : "less-loader"
], !develop);

exports.imageLoader = filename =>
    `url-loader?name=${filename}&limit=1024!image-webpack-loader`;
exports.fileLoader = filename =>
    `url-loader?name=${filename}&limit=1024`;
