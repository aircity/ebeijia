const webpack = require("webpack");
const base = require("./webpack.base.config");

module.exports = Object.assign({}, base, {
  target: "node",
  entry: "./src/server-entry.js",
  output: Object.assign({}, base.output, {
    filename: "server-bundle.js",
    libraryTarget: "commonjs2"
  }),
  module: {
    loaders: [
      {test: /\.css$/, loader: "ignore-loader"},
      {test: /\.less$/, loader: "ignore-loader"},
      ...base.module.loaders
    ]
  },
  vue: {
    loaders: {
      js: "babel-loader",
      css: "ignore-loader",
      less: "ignore-loader"
    }
  },
  externals: Object.keys(require("../package.json").dependencies),
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV || "development"),
      "process.env.VUE_ENV": '"server"'
    })
  ]
});
