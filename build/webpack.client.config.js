const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const base = require("./webpack.base.config");
const {cssLoader, lessLoader} = require("./utilities");

const config = Object.assign({}, base, {
  entry: {
    app: base.entry.app,
    vendor: [
      ...base.entry.vendor,
      "velocity-animate",
      "babel-regenerator-runtime"
    ]
  },
  module: {
    loaders: [
      {test: /\.css$/, loader: cssLoader},
      {test: /\.less$/, loader: lessLoader},
      ...base.module.loaders
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV || "development")
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      filename: "client-vendor-bundle.js"
    })
  ]
});

if (process.env.NODE_ENV === "production") {
  config.plugins.push(
    new ExtractTextPlugin("styles.css"),
    new webpack.LoaderOptionsPlugin({minimize: true}),
    new webpack.optimize.UglifyJsPlugin({compress: {warnings: false}})
  );
} else {
  config.devtool = "eval-source-map";
}

module.exports = config;
