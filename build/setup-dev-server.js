const path = require("path");
const webpack = require("webpack");
const devMiddleware = require("koa-webpack-dev-middleware");
const hotMiddleware = require("koa-webpack-hot-middleware");
const MFS = require("memory-fs");
const clientConfig = require("./webpack.client.config");
const serverConfig = require("./webpack.server.config");

module.exports = function(app, onUpdate) {
  clientConfig.entry.app = [
    "webpack-hot-middleware/client?reload=true",
    clientConfig.entry.app
  ];
  clientConfig.plugins.push(
    new webpack.HotModuleReplacementPlugin,
    new webpack.NoErrorsPlugin
  );

  const clientCompiler = webpack(clientConfig);
  app.use(devMiddleware(clientCompiler, {
    publicPath: clientConfig.output.publicPath,
    stats: {colors: true, chunks: false}
  }));
  app.use(hotMiddleware(clientCompiler));

  const serverCompiler = webpack(serverConfig);
  const mfs = new MFS;
  const outputPath = path.join(
    serverConfig.output.path,
    serverConfig.output.filename
  );
  serverCompiler.outputFileSystem = mfs;
  serverCompiler.watch({}, (error, stats) => {
    if (error) {
      throw error;
    }
    stats = stats.toJson();
    stats.errors.forEach(error => console.error(error));
    stats.warnings.forEach(error => console.warn(error));
    onUpdate(mfs.readFileSync(outputPath, "utf-8"));
  })
}
