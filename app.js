process.env.VUE_ENV = "server";
const isProd = process.env.NODE_ENV === "production";

const fs = require("fs");
const path = require("path");
const stream = require("stream");
const zlib = require("zlib");
const koa = require("koa");
const compress = require("koa-compress");
const cache = require("koa-static-cache");
const serialize = require("serialize-javascript");
const {createBundleRenderer} = require("vue-server-renderer");

const app = koa();

const html = (() => {
  const template = fs.readFileSync(path.resolve("./index.html"), "utf-8");
  const i = template.indexOf("{{ APP }}");
  const style = isProd
      ? '<link rel="stylesheet" href="/static/styles.css">'
      : "";
  return {
    head: template.slice(0, i).replace("{{ STYLE }}", style),
    tail: template.slice(i + "{{ APP }}".length)
  };
})();

let renderer;
if (isProd) {
  const bundlePath = path.resolve("static/server-bundle.js");
  renderer = createRenderer(fs.readFileSync(bundlePath, "utf-8"));
} else {
  require("./build/setup-dev-server")(app, bundle => {
    renderer = createRenderer(bundle);
  });
}

function createRenderer(bundle) {
  return createBundleRenderer(bundle, {
    cache: require("lru-cache")({
      max: 1000,
      maxAge: 1000 * 60 * 15
    })
  });
}

app.use(compress({flush: zlib.Z_SYNC_FLUSH}));
app.use(cache(path.resolve("static"), {
  prefix: "/static",
  buffer: true,
  maxAge: 0
}));
app.use(cache(path.resolve("public"), {
  maxAge: 0,
  buffer: true,
  dynamic: true
}));

app.use(function*(next) {
  if (this.url === "/") {
    this.set("Content-Type", "text/html");
    this.body = stream.PassThrough();
    if (!renderer) {
      return this.body.end("waiting for compilation...");
    }

    this.body.write(html.head);
    const context = {url: this.url};
    const renderStream = renderer.renderToStream(context);
    let firstChunk = true;

    renderStream.on("data", chunk => {
      if (firstChunk) {
        if (context.initialState) {
          this.body.write(
            `<script>window.__INITIAL_STATE__=${
              serialize(context.initialState, {isJSON: true})
            }</script>`
          )
        }
        firstChunk = false;
      }
      this.body.write(chunk);
    });

    renderStream.on("end", () => {
      this.body.end(html.tail);
    });

    renderStream.on("error", error => {
      throw error;
    });
  } else {
    yield next;
  }
});

app.listen(process.env.PORT || 9000);
